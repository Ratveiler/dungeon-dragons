﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : Creature
{ 
    public CircleCollider2D hitCollider;
    public float Score = 25f;
    //public AudioClip clip;
    void Update()
    {
        Vector2 velocity = _rigidbody.velocity;
        velocity.x = speed * transform.localScale.x * -1;
        _rigidbody.velocity = velocity;
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
        Knight knight = collider.gameObject.GetComponent<Knight>();

        if (knight != null)
        {
            animator.SetTrigger("Attack");
        }
        else
        {
            ChangeDirection();
        }
    }
    private void ChangeDirection()
    {
        if (transform.localScale.x < 0)
        {
            transform.localScale = Vector3.one;
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
    public void Attack()
    {
       
        GameController.Instance.AudioManager.PlaySoundRandomPitch("ogre1");

        Vector3 hitPosition = transform.TransformPoint(hitCollider.offset);

        DoHit(hitPosition, hitCollider.radius, damage);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Knight>() != null)
        {
            for (int i = 0; i < collision.contacts.Length; i++)
            {

                Vector2 fromDragonToContactVector = collision.contacts[i].point - (Vector2)transform.position;

                if (Vector2.Angle(fromDragonToContactVector, Vector2.up) < 45)
                {
                    Die();
                }
            }
        }
    }


    //public void Attack()
    //{

    //    // GetComponent<AudioSource>().PlayOneShot(clip);
    //    GameController.Instance.AudioManager.PlaySound("ogre1");
    //    Vector3 hitPosition = transform.TransformPoint(hitCollider.offset);
    //    Collider2D[] hits = Physics2D.OverlapCircleAll(hitPosition, hitCollider.radius);

    //    for (int i = 0; i < hits.Length; i++)
    //    {
    //        if (!GameObject.Equals(hits[i].gameObject, gameObject))
    //        {
    //            IDestructable destructable = hits[i].gameObject.GetComponent<IDestructable>();
    //            if (destructable != null)
    //            {
    //                destructable.Hit(damage);
    //            }
    //        }
    //    }
    //}


    //public void Die()
    //{
    //    // throw new System.NotImplementedException();
    //    Destroy(gameObject);
    //    ScoreLabel.score += Score;
    //}
} //[SerializeField] private float health = 100;
  //[SerializeField] private float speed;
  //public float damage;

//public float Health
//{
//    get { return health; }
//    set { health = value; }
//}

//private Rigidbody2D _rigidbody;
//private Animator animator;



//private void Awake()
//{
//   // animator = gameObject.GetComponentInChildren<Animator>();
//   //_rigidbody = GetComponent<Rigidbody2D>();
//}

// Start is called before the first frame update
//void Start()
//{

//}

// Update is called once per frame
//public void Hit(float damage)
//{
//    Health -= damage;
//    if (Health <= 0)
//    {
//        Die();
//    }
//    //throw new System.NotImplementedException();
//}