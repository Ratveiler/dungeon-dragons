﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InventoryUIButton : MonoBehaviour
{
   
    [SerializeField] private List<Sprite> sprites;
    public Text label;
    public Text count;
    public Image image;
    private InventoryUsedCallback callback;
    private InventoryItem itemData;
  
    public InventoryUsedCallback Callback { get => callback; set => callback = value; }
    public InventoryItem ItemData { get => itemData; set => itemData = value; }

    public void InventoryItemUsed(InventoryUIButton item)
    { }

    private void Start()
    {
        label.text = itemData.CrystallType.ToString();
        count.text = itemData.Quantity.ToString();

        string spriteNameToSearch = itemData.CrystallType.ToString().ToLower();
        image.sprite = sprites.Find(x => x.name.Contains(spriteNameToSearch));

        label.text = spriteNameToSearch;
        count.text = itemData.Quantity.ToString();

        gameObject.GetComponent<Button>().onClick.AddListener(() => callback(this));

    }
}
