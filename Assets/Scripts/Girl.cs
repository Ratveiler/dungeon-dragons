﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Girl : Creature
{
    public BoxCollider2D hitCollider;
    public float Score = 25f;
    //public AudioClip clip;
    void Update()
    {
        Vector2 velocity = _rigidbody.velocity;
        velocity.x = speed * transform.localScale.x * -1;
        _rigidbody.velocity = velocity;
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
        Gay gay = collider.gameObject.GetComponent<Gay>();

        if (gay != null)
        {
            animator.SetTrigger("Attack");
        }
        else
        {
            ChangeDirection();
        }
    }
    private void ChangeDirection()
    {
        if (transform.localScale.x < 0)
        {
            transform.localScale = Vector3.one;
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
    public void Attack()
    {

        GameController.Instance.AudioManager.PlaySound("helpme");

        Vector3 hitPosition = transform.TransformPoint(hitCollider.offset);

        DoHit(hitPosition, hitCollider.shapeCount, damage);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Gay>() != null)
        {
            for (int i = 0; i < collision.contacts.Length; i++)
            {

                Vector2 fromDragonToContactVector = collision.contacts[i].point - (Vector2)transform.position;

                if (Vector2.Angle(fromDragonToContactVector, Vector2.up) < 45)
                {
                    Die();
                }
            }
        }
    }
    
}