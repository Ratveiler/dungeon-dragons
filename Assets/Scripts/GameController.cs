﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using System;
public enum MyValues { FirstValue, SecondValue, ThirdValue }
public enum GameState { Play, Pause }

public delegate void InventoryUsedCallback(InventoryUIButton item);
public delegate void UpdateHeroParametersHandler(HeroParameters parameters);


public class GameController : MonoBehaviour
{
    private static GameController _instance;
    public event UpdateHeroParametersHandler OnUpdateHeroParameters;
    private GameState state;
    private int score;
    [SerializeField] private List<InventoryItem> inventory;
    [SerializeField] private HeroParameters hero;

    private Knight knight;
    private Gay gay;

    [SerializeField] private int dragonHitScore;
    [SerializeField] private int dragonKillScore;
    [SerializeField] private int dragonKillExperience;



    public Audio audioManager;


    public Knight Knight { get => knight; set => knight = value; }
    public Audio AudioManager { get => audioManager; set => audioManager = value; }
    public List<InventoryItem> Inventory { get => inventory; set => inventory = value; }
    public HeroParameters Hero { get => hero; set => hero = value; }
    public Gay Gay { get => gay; set => gay = value; }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            if (_instance != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);

        state = GameState.Play;
        Inventory = new List<InventoryItem>();
        InitializeAudioManager();
    }

    private void InitializeAudioManager()
    {
        audioManager.SourceSFX = gameObject.AddComponent<AudioSource>();
        audioManager.SourceMusic = gameObject.AddComponent<AudioSource>();
        audioManager.SourceRandomPitchSFX = gameObject.AddComponent<AudioSource>();
        gameObject.AddComponent<AudioListener>();
    }

    private int Score
    {
        get
        {
            return score;
        }
        set
        {
            if (value != score)
            {
                score = value;
                HUD.Instance.SetScore(score.ToString());
            }
        }
    }

    public void StartNewLevel()
    {
        HUD.Instance.SetScore(Score.ToString());

        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
        }

        state = GameState.Play;
    }

    public void Hit(IDestructable victim)
    {
        if (victim.GetType() == typeof(Dragon))
        {
            //дракон получил урон
            Score += dragonHitScore;
        }
        if (victim.GetType() == typeof(Girl))
        {
            Score += dragonHitScore;
        }

        if (victim.GetType() == typeof(Knight))
        {
            HUD.Instance.HealthBar.value = victim.Health;
        }
        if (victim.GetType() == typeof(Gay))
        {
            HUD.Instance.HealthBar.value = victim.Health;
        }
    }

    public void Killed(IDestructable victim)
    {
        if (victim.GetType() == typeof(Dragon))
        {
            Score += dragonKillScore;
            hero.Experience += dragonKillExperience;
            Destroy((victim as MonoBehaviour).gameObject);
        }
        if (victim.GetType() == typeof(Girl))
        {
            Score += dragonKillScore;
            hero.Experience += dragonKillExperience;
            Destroy((victim as MonoBehaviour).gameObject);
        }

        if (victim.GetType() == typeof(Knight))
        {
            GameOver();
        }
        if (victim.GetType() == typeof(Gay))
        {
            GameOver();
        }
    }

    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameController =
                Instantiate(Resources.Load("Prefabs/GameController")) as GameObject;
                _instance = gameController.GetComponent<GameController>();
            }

            return _instance;
        }
    }


    public GameState State
    {
        get
        {
            return state;
        }
        set
        {
            if (value == GameState.Play)
            {
                Time.timeScale = 1.0f;
            }
            else
            {
                Time.timeScale = 0.0f;
            }
            state = value;
        }
    }



    public void AddNewInventoryItem(InventoryItem itemData)
    {
        InventoryUIButton newUiButton = HUD.Instance.AddNewInventoryItem(itemData);
        InventoryUsedCallback callback = new InventoryUsedCallback(InventoryItemUsed);
        newUiButton.Callback = callback;
        Inventory.Add(itemData);
    }


    public void InventoryItemUsed(InventoryUIButton item)
    {
        switch (item.ItemData.CrystallType)
        {
            case CrystallType.Blue:
                //убираем
                //knight.Speed += item.ItemData.Quantity / 10f;
                //добавляем
                hero.Speed += item.ItemData.Quantity / 10f;
                break;

            case CrystallType.Red:
                //убираем
                //knight.Damage += item.ItemData.Quantity / 10f;
                //добавляем
                hero.Damage += item.ItemData.Quantity / 2f;
                break;

            case CrystallType.Green:
                //убираем
                //MaxHealth += item.ItemData.Quantity / 10f;
                //knight.Health = MaxHealth;
                //HUD.Instance.HealthBar.maxValue = MaxHealth;
                //HUD.Instance.HealthBar.value = MaxHealth;  
                //добавляем
                hero.MaxHealth += item.ItemData.Quantity / 0.2f;
                break;

            default:
                Debug.LogError("Wrong crystall type!");
                break;
        }

        Inventory.Remove(item.ItemData);
        Destroy(item.gameObject);
        AudioManager.PlaySound("bottle");

        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
        }
    }


    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        GameController.Instance.State = GameState.Play;
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        GameController.Instance.State = GameState.Play;
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        //GameController.Instance.State = GameState.Play;
    }
    public void PrincessFound()
    {
        HUD.Instance.ShowLevelWonWindow();
        // AudioManager.PlaySound("Crossbowman_See_001");
    }
    public void GameOver()
    {
        HUD.Instance.ShowLevelLoseWindow();
        // AudioManager.PlaySound("ba8dfc082517587");
    }


    public void LevelUp()
    {
        if (OnUpdateHeroParameters != null)
        {
            OnUpdateHeroParameters(hero);
            AudioManager.PlaySound("Crossbowman_See_001");
        }
    }































}