﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
public class Knight : Creature
{

    [SerializeField] private float jumpForce = 250f;
    [SerializeField] private Transform GroundCheck;
    [SerializeField] private float stairSpeed = 2f;
    [SerializeField] private Transform attackPoint;
    [SerializeField] private float attackRange;
    [SerializeField] private float hitDelay;
    public VariableJoystick variableJoystick;



    private bool onStair;
    private bool onGround;





    public bool OnStair
    {
        get { return onStair; }
        set
        {
            if (value == true)
            {
                _rigidbody.gravityScale = 0;
            }
            else
            {
                _rigidbody.gravityScale = 2;
            }
            onStair = value;
        }
    }

    public float Damage { get => damage; set => damage = value; }
    public float Speed { get => speed; set => speed = value; }

    private void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        Health = parameters.MaxHealth;
        Damage = parameters.Damage;
        Speed = parameters.Speed;

    }


    private void Start()
    {
        //health = GameController.Instance.MaxHealth;
        GameController.Instance.Knight = this;
        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;

    }


    void Update()
    {
        onGround = CheckGround();
        animator.SetBool("Jump", !onGround);
        if (CrossPlatformInputManager.GetButtonDown("Jump") && onGround)
        {


            _rigidbody.AddForce(Vector2.up * jumpForce);
        }
        // animator.SetFloat("Walk", Mathf.Abs(Input.GetAxis("Horizontal")));
        // velocity.x = Input.GetAxis("Horizontal") * speed; _rigidbody.velocity = velocity;

        animator.SetFloat("Walk", Mathf.Abs(variableJoystick.Horizontal));
        Vector2 velocity = _rigidbody.velocity;
        velocity.x = variableJoystick.Horizontal * Speed; _rigidbody.velocity = velocity;
        if (transform.localScale.x < 0)
        {
            if (variableJoystick.Horizontal > 0)
            {
                transform.localScale = Vector3.one;
            }
        }
        else
        {
            if (variableJoystick.Horizontal < 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
        }


        Attack();




        if (OnStair)
        {
            velocity = _rigidbody.velocity;
            //velocity.y = Input.GetAxis("Vertical") * stairSpeed;
            velocity.y = variableJoystick.Vertical * stairSpeed;
            _rigidbody.velocity = velocity;
        }
    }


    //     //   _rigidbody.AddForce(Vector2.up * jumpForce);

    //void Jump()
    //{
    //    _rigidbody.AddForce(Vector2.up * jumpForce);
    //}


    public void Attack()
    {
        if (CrossPlatformInputManager.GetButtonDown("Attack"))
        //  if (CrossPlatformInputManager.GetButtonDown("Attack"))
        {

            animator.SetTrigger("Attack");
            if (Time.timeScale > 0.2)
            {
                GameController.Instance.AudioManager.PlaySoundRandomPitch("swing3");
            }
            Invoke("Attack", hitDelay);
            DoHit(attackPoint.position, attackRange, Damage);
        }
    }


    private bool CheckGround()
    {
        RaycastHit2D[] hits = Physics2D.LinecastAll(transform.position, GroundCheck.position);

        for (int i = 0; i < hits.Length; i++)
        {
            if (!GameObject.Equals(hits[i].collider.gameObject, gameObject))
            {
                return true;
            }
        }
        return false;
    }

    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -=
                        HandleOnUpdateHeroParameters;
    }

}
