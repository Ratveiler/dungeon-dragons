﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public string SampleScene;
    
    public void RestartScene()
    {
        SceneManager.LoadScene(SampleScene);
    }
}
