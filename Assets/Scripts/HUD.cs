﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{

    [SerializeField] private GameObject inventoryWindow;
    [SerializeField] private GameObject LevelWonWindow;
    [SerializeField] private GameObject LevelLoseWindow;
    [SerializeField] private GameObject InGameMenu;
    [SerializeField] private GameObject optionsWindow;
    [SerializeField] private Text damageValue;
    [SerializeField] private Text healthValue;
    [SerializeField] private Text speedValue;

    static private HUD _instance;
    public Text scoreLabel;
    public Slider HealthBar;
    public InventoryUIButton inventoryItemPrefab;
    public Transform inventoryContainer;

    public static HUD Instance
    {
        get
        {
            return _instance;
        }
    }

    public Text DamageValue { get => damageValue; set => damageValue = value; }
    public Text HealthValue { get => healthValue; set => healthValue = value; }
    public Text SpeedValue { get => speedValue; set => speedValue = value; }

    private void Awake()
    {
        _instance = this;
    }
    private void Start()
    {
        LoadInventory();

        GameController.Instance.OnUpdateHeroParameters += HandleOnUpdateHeroParameters;
        GameController.Instance.StartNewLevel();
    }

    private void HandleOnUpdateHeroParameters(HeroParameters parameters)
    {
        HealthBar.maxValue = parameters.MaxHealth;
        HealthBar.value = parameters.MaxHealth;
        UpdateCharacterValues(parameters.MaxHealth, parameters.Speed, parameters.Damage);
    }


    public void UpdateCharacterValues(float newHealth, float newSpeed, float newDamage)
    {
        healthValue.text = newHealth.ToString();
        speedValue.text = newSpeed.ToString();
        damageValue.text = newDamage.ToString();
    }


    public void ShowWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", true);
        GameController.Instance.State = GameState.Pause;
        GameController.Instance.AudioManager.PlaySound("door");
    }

    public void HideWindow(GameObject window)
    {
        window.GetComponent<Animator>().SetBool("Open", false);
        GameController.Instance.State = GameState.Play;
        GameController.Instance.AudioManager.PlaySound("door");
    }
    public void SetScore(string scoreValue)
    {
        scoreLabel.text = scoreValue;
    }
    public InventoryUIButton AddNewInventoryItem(InventoryItem itemData)
    {
        InventoryUIButton newItem = Instantiate(inventoryItemPrefab) as InventoryUIButton;
        newItem.transform.SetParent(inventoryContainer);
        newItem.ItemData = itemData;
        return newItem;
    }
    public void LoadInventory()
    {
        InventoryUsedCallback callback = new
        InventoryUsedCallback(GameController.Instance.InventoryItemUsed);

        for (int i = 0; i < GameController.Instance.Inventory.Count; i++)
        {
            InventoryUIButton newItem = AddNewInventoryItem(GameController.Instance.Inventory[i]);
            newItem.Callback = callback;
        }

    }

    public void ButtonNext()
    {
        GameController.Instance.LoadNextLevel();
    }

    public void ButtonRestart()
    {
        GameController.Instance.RestartLevel();
    }

    public void ButtonMainMenu()
    {
        GameController.Instance.LoadMainMenu();
    }
    public void ShowLevelWonWindow()
    {
        ShowWindow(LevelWonWindow);
        GameController.Instance.State = GameState.Pause;
    }
    public void ShowLevelLoseWindow()
    {
        ShowWindow(LevelLoseWindow);
        GameController.Instance.State = GameState.Pause;
    }
    private void OnDestroy()
    {
        GameController.Instance.OnUpdateHeroParameters -=
        HandleOnUpdateHeroParameters;
    }

    public void SetSoundVolume(Slider slider)
    {
        GameController.Instance.AudioManager.SfxVolume = slider.value;
    }
    public void SetMusicVolume(Slider slider)
    {
        GameController.Instance.AudioManager.MusicVolume = slider.value;
    }
}
