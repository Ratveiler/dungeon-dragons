﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsUI : MonoBehaviour
{
    public Text label;
    public static float coin;

    private static float Coin1;
    public static float Coin { get => Coin1; set => Coin1 = value; }

    void Start()
    {
        label = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        label.text = coin.ToString();
    }
}
